<?php
 /**
  *
  * @package System
  *
  */

//==========================================================
function get_url_infos($url){
//==========================================================

    $data=parse_url($url);
    if (array_key_exists('query', $data)) {
        parse_str($data['query'], $queryParameters);
        $data["query"]= $queryParameters;
    }
    return $data;

}

//=================================================================
class SystemBase extends ServerNode {
//=================================================================
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
    }
    //-------------------------------------------------------------
    function build_url($action,$file) {
       return $this->parent->build_url($action,$file);
    }
    //-------------------------------------------------------------
    function read(){

    }
    //-------------------------------------------------------------
    function write($obj){

    }
    //-------------------------------------------------------------
    function delete(){

    }
    //-------------------------------------------------------------

}
//=================================================================
class SystemFile extends SystemBase {
//=================================================================
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
    }
    //-------------------------------------------------------------
    function read(){
        $path=$this->filepath();
        $myfile = fopen($path, "r") or die("Unable to open file!");
        echo fread($myfile, filesize($path));
        fclose($myfile);

    }

    //-------------------------------------------------------------
    function write($obj){
        $path=$this->filepath();
        $txt=json_encode($obj);
        $myfile = fopen($path, "w") or die("Unable to open file!");
        fwrite($myfile, $txt);
        fclose($myfile);
    }
    //-------------------------------------------------------------
}
//=================================================================
class SystemDir extends SystemBase {
//=================================================================
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
        new InitDirs($this,["event"=>"setup","cls"=>"SystemDir"]);
        new InitFiles($this,["event"=>"setup","cls"=>"SystemFile"]);
    }
    //-------------------------------------------------------------
    function read(){

        $files =[];
        foreach($this->search("*/[SystemFile]")->iter() as $file){
            $links=[];
            $links["read"]=$this->build_url("read",$file);
            $links["delete"]=$this->build_url("delete",$file);
            $files[$file->get("filename")]=$links;
        }
        echo json_encode($files);

    }
    //-------------------------------------------------------------
    function write($obj){
        $path=$this->filepath();
         $t=time();
        $name=date("Y_m_d_H_i_s",$t);
        $filename=$path."/".$name.".json";
        $node=new SystemFile($this,["path"=>$filename]);
        $node->write($obj);
    }
    //-------------------------------------------------------------

}
//=================================================================
class System extends SystemDir {
//=================================================================
    //-------------------------------------------------------------
    function onInit() {
        parent::onInit();
    }
    //-------------------------------------------------------------
    function build_url($action,$file) {
        return "http://nox/test1.php?action=".$action."&select=".$file->path();
       //return $this->get("url");
    }
    //-------------------------------------------------------------
    function call() {
        global $_SERVER;
        global $_POST;
        global $_GET;

        if($_SERVER['REQUEST_METHOD'] == 'POST') {

            //$request=$_POST;
            $txt = file_get_contents('php://input');
            $this->make_update($txt);

        }else{
//action

            $request=new BaseClass($_GET);
            $action=$request->get("action","read");
            if($action=="read"){
                $this->make_read($request);
            }else if($action=="create"){
                $this->make_create($request);
            }else if($action=="delete"){
                $this->make_delete($request);
            }
        }

    }
    //-------------------------------------------------------------
    function make_create($request){
//select
        $dir=$this->get('path')."/".$request->get("select")."/".$request->get("name");
        if( ! is_dir($dir) ){
            mkdir($dir, 0777, true);
        }
    }
    //-------------------------------------------------------------
    function make_read($request){

        $node=$this->find($request->get("select","/"));
        if($node){
        $node->read();
        }else{
            echo "no path ".$request->get("select","/");
        }
    }
    //-------------------------------------------------------------
    function make_update($txt){
//select
//data
        $obj = json_decode($txt); 

        $select=$obj->select;
        $node=$this->find($select);

        if($node){
            $obj->date=getdate();
            $node->write($obj);
        }else{
            echo "no node ".$select;
        }

    }

    //-------------------------------------------------------------
    function make_delete($request){
        $node=$this->find($request->get("select","/"));
        $node->delete();
    }
    //-------------------------------------------------------------


}
//=================================================================


?>
